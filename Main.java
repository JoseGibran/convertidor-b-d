package org.josepolonsky;
import org.josepolonsky.util.*;
import org.josepolonsky.manejoDeDatos.Convertidor;
public class Main{
	public static void main(String args[]){
		IngresoDatos entrada=new IngresoDatos();
		Convertidor convertidor=new Convertidor();
		Pantalla pantalla=new Pantalla();
		String binario;
		int bin=0;
		int menu=0;
		int numero=0;
		do{
		System.out.println("++===================++");
		System.out.println("||    Convertidor    ||");
		System.out.println("++===================++");
		System.out.println("");
		System.out.println("||1...Binario-Decimal||");
		System.out.println("||2...Decimal-Binario||");
		System.out.println("||0...Salir          ||");
		System.out.println("++===================++");
		pantalla.limpiarPantalla(16);
		try{
		System.out.print("R:// ");
		menu=Integer.parseInt(entrada.leerDatos());
		}catch(NumberFormatException nfe){
			System.out.println("Error de entrada, trate de nuevo");
		}
		switch(menu){
		case 1:
		do{
			System.out.println("*************************");
			System.out.println("*****Binario-Decimal*****");
			System.out.println("*************************");
			pantalla.limpiarPantalla(20);
			System.out.println("Ingrese un Numero Binario");
			System.out.print("R:// ");
			binario=entrada.leerDatos();
			System.out.println(binario);
			try{
				System.out.println("++==========++");
				System.out.println("||Resultado:||");
				System.out.println("++==========++");
				System.out.println("binario: "+binario);
				System.out.println("==============================================================================");
				System.out.println("Decimal: "+convertidor.convertirBinario(binario));
				System.out.println("==============================================================================");
				pantalla.limpiarPantalla(16);
				System.out.println("Presione Enter para continuar...");
				String enter=entrada.leerDatos();
				bin=Integer.parseInt(binario);
			}catch(NumberFormatException nfe){
				System.out.println("Debe ser un numero Binario");
			}
		}while(bin==0);
		break;
		case 2:
		do{
		numero=0;
			System.out.println("*************************");
			System.out.println("*****Decimal-Binario*****");
			System.out.println("*************************");
			pantalla.limpiarPantalla(20);
			System.out.println("Ingrese un Numero Decimal");
			System.out.print("R:// ");
		try{
		numero=Integer.parseInt(entrada.leerDatos());
		convertidor.convertirDecimal(numero);
		}catch(NumberFormatException nfe){
			System.out.println("Debe ingresar un numero Decimal");
		}
		}while(numero==0);
		break;
		}
		}while(menu>0);
	}
}