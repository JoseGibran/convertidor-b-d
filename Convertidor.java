package org.josepolonsky.manejoDeDatos;
import java.util.List;
import java.util.ArrayList;
import org.josepolonsky.util.*;
public class Convertidor{
	String binario;
	int total;
	int bin;
	IngresoDatos entrada;
	Pantalla pantalla=new Pantalla();
	public Convertidor(){
		binario=null;
		total=0;
		bin=0;
		entrada=new IngresoDatos();
	}
	public boolean comparar(String binario){
		boolean incorrecto=false;
		
		for(int i=0; i<binario.length(); i++){
			bin=Integer.parseInt(binario.substring(i,i+1));
			if(bin>1){
				incorrecto=true;
				break;
			}

		}
			return incorrecto;
	}
	
	public int convertirBinario(String binario){
		boolean incorrecto=false;
		total=0;
		bin=0;
		do{
		incorrecto=comparar(binario);
			if(incorrecto==true){
				System.out.println("Ingrese un nuevo Valor");
				System.out.println("Debe ser un numero binario");
				System.out.print("R:// ");
				binario=entrada.leerDatos();		
			}
		}while(incorrecto==true);
		for(int i=0; i<binario.length(); i++){
			bin=Integer.parseInt(binario.substring(i,i+1));
			total=total*2+bin;
		}
		return total;
	}
	public void convertirDecimal(int decimal){
		//ArrayList <Integer> numero=new ArrayList<Integer>();
		int numero[];
		numero= new int[100];
		bin=0;
		total=0;
		int count=0;
		do{
			total=decimal/2;
			bin=decimal%2;
			decimal=total;
			numero[count]=bin;
			count++;
		}while(total>0);
				System.out.println("++==========++");
				System.out.println("||Resultado:||");
				System.out.println("++==========++");
		System.out.println("Binario: ");
		for(int i=count; i>=0; i--){
			System.out.print(numero[i]);
		}
		System.out.println("");
		pantalla.limpiarPantalla(18);
		System.out.println("Presione Enter para continuar...");
		String enter=entrada.leerDatos();
		
	}
	
}
