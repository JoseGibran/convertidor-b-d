package org.josepolonsky.util;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
public class IngresoDatos{
	String dato;
	public IngresoDatos(){
		dato=null;
	}
	public String leerDatos(){
		BufferedReader entrada=new BufferedReader(new InputStreamReader(System.in));
		try{
			dato=entrada.readLine();
		}catch(IOException ioe){
		
		}
		return dato;
	}
}